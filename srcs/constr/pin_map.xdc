# SPI interface
set_property IOSTANDARD LVDS_25 [get_ports MISO_miso_p]
set_property IOSTANDARD LVDS_25 [get_ports MISO_miso_n]
set_property PACKAGE_PIN AG26 [get_ports MISO_miso_p]

set_property IOSTANDARD LVDS_25 [get_ports spi_clock_clk_p]
set_property IOSTANDARD LVDS_25 [get_ports spi_clock_clk_n]
set_property PACKAGE_PIN T29 [get_ports spi_clock_clk_p]

set_property IOSTANDARD LVDS_25 [get_ports MOSI_mosi_p]
set_property IOSTANDARD LVDS_25 [get_ports MOSI_mosi_n]
set_property PACKAGE_PIN V28 [get_ports MOSI_mosi_p]

set_property IOSTANDARD LVDS_25 [get_ports {SS_ss_p[0]}]
set_property IOSTANDARD LVDS_25 [get_ports {SS_ss_n[0]}]
set_property PACKAGE_PIN R28 [get_ports {SS_ss_p[0]}]

#Clock input from SI chip

set_property PACKAGE_PIN AE22 [get_ports CLK_SI_8_clk_p]
set_property PACKAGE_PIN AF22 [get_ports CLK_SI_8_clk_n]
set_property DIFF_TERM true [get_ports CLK_SI_8_clk_*]
set_property IOSTANDARD LVDS_25 [get_ports CLK_SI_8_clk_*]

#CMD line to atlaspix3
set_property PACKAGE_PIN AD23 [get_ports CMD_atp3_cmd_p]
set_property PACKAGE_PIN AE23 [get_ports CMD_atp3_cmd_n]
set_property DIFF_TERM true [get_ports CMD_atp3_cmd_*]
set_property IOSTANDARD LVDS_25 [get_ports CMD_atp3_cmd_*]

# clock to ATLASPix3
set_property IOSTANDARD LVDS_25 [get_ports CLKEXT_clk_*]
set_property PACKAGE_PIN AG24 [get_ports CLKEXT_clk_p]

set_property IOSTANDARD LVDS_25 [get_ports CLKREF_clk_*]
set_property PACKAGE_PIN AC24 [get_ports CLKREF_clk_p]


#led indicator of data lock
set_property PACKAGE_PIN A17 [get_ports led_0]
set_property IOSTANDARD LVCMOS15 [get_ports led_0]

#Pulser
set_property IOSTANDARD LVDS_25 [get_ports PULSE1_ext_pulse_out_1_p]
set_property IOSTANDARD LVDS_25 [get_ports PULSE1_ext_pulse_out_1_n]
set_property PACKAGE_PIN AE18 [get_ports PULSE1_ext_pulse_out_1_p]
set_property PACKAGE_PIN AE17 [get_ports PULSE1_ext_pulse_out_1_n]


#reset for CLK to GPIO button
set_property PACKAGE_PIN K15 [get_ports reset]
set_property IOSTANDARD LVCMOS15 [get_ports reset]


# Control pads ATP3BITS_atp3_invert_p

set_property DIFF_TERM true [get_ports ATP3BITS_atp3_invert*]
set_property IOSTANDARD LVDS_25 [get_ports ATP3BITS_atp3_invert_p]
set_property PACKAGE_PIN R25 [get_ports ATP3BITS_atp3_invert_p]
set_property PACKAGE_PIN R26 [get_ports ATP3BITS_atp3_invert_n]


set_property DIFF_TERM true [get_ports ATP3BITS_atp3_interface_speed_*]
set_property IOSTANDARD LVDS_25 [get_ports ATP3BITS_atp3_interface_speed_p]
set_property IOSTANDARD LVDS_25 [get_ports ATP3BITS_atp3_interface_speed_n]
set_property PACKAGE_PIN T30 [get_ports ATP3BITS_atp3_interface_speed_p]
set_property PACKAGE_PIN U30 [get_ports ATP3BITS_atp3_interface_speed_n]


set_property DIFF_TERM true [get_ports ATP3BITS_atp3_always_enb*]
set_property IOSTANDARD LVDS_25 [get_ports ATP3BITS_atp3_always_enb_p]
set_property IOSTANDARD LVDS_25 [get_ports ATP3BITS_atp3_always_enb_n]
set_property PACKAGE_PIN W25 [get_ports ATP3BITS_atp3_always_enb_p]
set_property PACKAGE_PIN W26 [get_ports ATP3BITS_atp3_always_enb_n]

set_property DIFF_TERM true [get_ports ATP3BITS_atp3_takefast*]
set_property IOSTANDARD LVDS_25 [get_ports ATP3BITS_atp3_takefast_p]
set_property IOSTANDARD LVDS_25 [get_ports ATP3BITS_atp3_takefast_n]
set_property PACKAGE_PIN P30 [get_ports ATP3BITS_atp3_takefast_p]
set_property PACKAGE_PIN R30 [get_ports ATP3BITS_atp3_takefast_n]



# triggering

set_property DIFF_TERM true [get_ports trigger_output_*]
set_property IOSTANDARD LVDS_25 [get_ports trigger_output_p]
set_property IOSTANDARD LVDS_25 [get_ports trigger_output_n]
set_property PACKAGE_PIN Y22 [get_ports trigger_output_p]
set_property PACKAGE_PIN Y23 [get_ports trigger_output_n]


set_property DIFF_TERM true [get_ports hitbus_input_*]
set_property IOSTANDARD LVDS_25 [get_ports hitbus_input_p]
set_property IOSTANDARD LVDS_25 [get_ports hitbus_input_n]
set_property PACKAGE_PIN P21 [get_ports hitbus_input_p]
set_property PACKAGE_PIN R21 [get_ports hitbus_input_n]


#reset for CLK to GPIO button
set_property PACKAGE_PIN AD19 [get_ports trigger_input]
set_property IOSTANDARD LVCMOS18 [get_ports trigger_input]
#reset for CLK to GPIO button
set_property PACKAGE_PIN AD18 [get_ports busy[0]]
set_property IOSTANDARD LVCMOS18 [get_ports busy[0]]

#--------------- ATLASPix3 data mode ----------------------
#GTX clock from CaR Board
#set_property PACKAGE_PIN AD10 [get_ports GTX_CLK_clk_p]
#set_property PACKAGE_PIN AD9 [get_ports GTX_CLK_clk_n]

# data from ATLASPix3
#set_property PACKAGE_PIN AH9 [get_ports {data_in_grx_n}]
#set_property PACKAGE_PIN AH10 [get_ports {data_in_grx_p}]
#----------------------------------------------------------

#--------------- loopback  mode --------------------------
#GTX clock from ZC706 SMA
set_property PACKAGE_PIN W8 [get_ports GTX_CLK_clk_p]
set_property PACKAGE_PIN W7 [get_ports GTX_CLK_clk_n]

# data from loopback
set_property PACKAGE_PIN AB5 [get_ports {data_in_grx_n[0]}]
set_property PACKAGE_PIN AB6 [get_ports {data_in_grx_p[0]}]

# data source for loopback via SMA
set_property PACKAGE_PIN Y2 [get_ports {txp[0]}]
set_property PACKAGE_PIN Y1 [get_ports {txn[0]}]


set_property LOC GTXE2_CHANNEL_X0Y2 [get_cells ATLASPix3BD_i/ATLASPix3_readout/ATLASPix3_Aurora6466b_0/U0/ATLASPix3_Aurora6466b_v1_0_S00_AXI_inst/auroratx/inst/aurora_64b66b_tx_sim_core_i/aurora_64b66b_tx_sim_wrapper_i/aurora_64b66b_tx_sim_multi_gt_i/aurora_64b66b_tx_sim_gtx_inst/gtxe2_i]
set_property LOC GTXE2_CHANNEL_X0Y0 [get_cells ATLASPix3BD_i/ATLASPix3_readout/ATLASPix3_Aurora6466b_0/U0/ATLASPix3_Aurora6466b_v1_0_S00_AXI_inst/ATP3_RCV/inst/aurora_64b66b_0_wrapper_i/aurora_64b66b_0_multi_gt_i/aurora_64b66b_0_gtx_inst/gtxe2_i ]
set_property LOC IBUFDS_GTE2_X0Y0 [get_cells ATLASPix3BD_i/ATLASPix3_readout/ATLASPix3_Aurora6466b_0/U0/ATLASPix3_Aurora6466b_v1_0_S00_AXI_inst/auroratx/inst/IBUFDS_GTXE2_CLK1]
#----------------------------------------------------------

