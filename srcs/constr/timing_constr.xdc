set_clock_groups -name async_clocks -asynchronous -group [get_clocks clk_fpga_0] -group [get_clocks -of_objects [get_pins ATLASPix3BD_i/clock_distribution/clk_wiz_0/inst/plle2_adv_inst/CLKOUT0]]
set_clock_groups -name async_clocks -asynchronous -group [get_clocks -of_objects [get_pins ATLASPix3BD_i/clock_distribution/clk_wiz_0/inst/plle2_adv_inst/CLKOUT0]] -group [get_clocks clk_fpga_0]
set_max_delay -from [get_clocks clk_fpga_0] -to [get_clocks clk_fpga_0] 5.000


set_false_path -from [get_clocks clk_fpga_0] -to [get_clocks -of_objects [get_pins ATLASPix3BD_i/clock_distribution/clk_wiz_0/inst/plle2_adv_inst/CLKOUT1]]
set_max_delay -from [get_pins ATLASPix3BD_i/Core/processing_system7_0/inst/PS7_i/MAXIGP0ACLK] -to [get_pins {ATLASPix3BD_i/Control_interfaces/ATLASPix3_ControlBit_0/U0/ATLASPix3_ControlBit_Interface_v1_0_S00_AXI_inst/slv_reg4_reg[0]/CE}] 4.100
set_max_delay -from [get_pins ATLASPix3BD_i/Core/processing_system7_0/inst/PS7_i/MAXIGP0ACLK] -to [get_pins ATLASPix3BD_i/Core/processing_system7_0/inst/PS7_i/MAXIGP0WREADY] 4.100
set_false_path -from [get_pins {ATLASPix3BD_i/Control_interfaces/ATLASPix3_ControlBit_0/U0/ATLASPix3_ControlBit_Interface_v1_0_S00_AXI_inst/slv_reg4_reg[0]/C}] -to [get_pins ATLASPix3BD_i/clock_distribution/clk_wiz_0/inst/clkout2_buf/CE0]



set_false_path -from [get_clocks clk_fpga_0] -to [get_clocks -of_objects [get_pins ATLASPix3BD_i/clock_distribution/clk_wiz_0/inst/plle2_adv_inst/CLKOUT3]]
set_property C_CLK_INPUT_FREQ_HZ 300000000 [get_debug_cores dbg_hub]
set_property C_ENABLE_CLK_DIVIDER false [get_debug_cores dbg_hub]
set_property C_USER_SCAN_CHAIN 1 [get_debug_cores dbg_hub]
connect_debug_port dbg_hub/clk [get_nets clk]
