--Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2018.2 (lin64) Build 2258646 Thu Jun 14 20:02:38 MDT 2018
--Date        : Tue Oct 22 10:03:05 2019
--Host        : pc-unige-peary running 64-bit Ubuntu 16.04.6 LTS
--Command     : generate_target ATLASPix3BD_wrapper.bd
--Design      : ATLASPix3BD_wrapper
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity ATLASPix3BD_wrapper is
  port (
    CLK_EXT_N : out STD_LOGIC_VECTOR ( 0 to 0 );
    CLK_EXT_P : out STD_LOGIC_VECTOR ( 0 to 0 );
    CLK_REF_N : out STD_LOGIC_VECTOR ( 0 to 0 );
    CLK_REF_P : out STD_LOGIC_VECTOR ( 0 to 0 );
    CLK_SI_8_clk_n : in STD_LOGIC;
    CLK_SI_8_clk_p : in STD_LOGIC;
    DDR_addr : inout STD_LOGIC_VECTOR ( 14 downto 0 );
    DDR_ba : inout STD_LOGIC_VECTOR ( 2 downto 0 );
    DDR_cas_n : inout STD_LOGIC;
    DDR_ck_n : inout STD_LOGIC;
    DDR_ck_p : inout STD_LOGIC;
    DDR_cke : inout STD_LOGIC;
    DDR_cs_n : inout STD_LOGIC;
    DDR_dm : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_dq : inout STD_LOGIC_VECTOR ( 31 downto 0 );
    DDR_dqs_n : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_dqs_p : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_odt : inout STD_LOGIC;
    DDR_ras_n : inout STD_LOGIC;
    DDR_reset_n : inout STD_LOGIC;
    DDR_we_n : inout STD_LOGIC;
    FIXED_IO_ddr_vrn : inout STD_LOGIC;
    FIXED_IO_ddr_vrp : inout STD_LOGIC;
    FIXED_IO_mio : inout STD_LOGIC_VECTOR ( 53 downto 0 );
    FIXED_IO_ps_clk : inout STD_LOGIC;
    FIXED_IO_ps_porb : inout STD_LOGIC;
    FIXED_IO_ps_srstb : inout STD_LOGIC;
    led_0 : out STD_LOGIC;
    miso_0 : in STD_LOGIC;
    mosi_0 : out STD_LOGIC;
    reset : in STD_LOGIC;
    spi_clk_0 : out STD_LOGIC;
    ss_0 : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
end ATLASPix3BD_wrapper;

architecture STRUCTURE of ATLASPix3BD_wrapper is
  component ATLASPix3BD is
  port (
    reset : in STD_LOGIC;
    led_0 : out STD_LOGIC;
    CLK_REF_P : out STD_LOGIC_VECTOR ( 0 to 0 );
    CLK_REF_N : out STD_LOGIC_VECTOR ( 0 to 0 );
    CLK_EXT_P : out STD_LOGIC_VECTOR ( 0 to 0 );
    CLK_EXT_N : out STD_LOGIC_VECTOR ( 0 to 0 );
    miso_0 : in STD_LOGIC;
    spi_clk_0 : out STD_LOGIC;
    ss_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
    mosi_0 : out STD_LOGIC;
    CLK_SI_8_clk_n : in STD_LOGIC;
    CLK_SI_8_clk_p : in STD_LOGIC;
    DDR_cas_n : inout STD_LOGIC;
    DDR_cke : inout STD_LOGIC;
    DDR_ck_n : inout STD_LOGIC;
    DDR_ck_p : inout STD_LOGIC;
    DDR_cs_n : inout STD_LOGIC;
    DDR_reset_n : inout STD_LOGIC;
    DDR_odt : inout STD_LOGIC;
    DDR_ras_n : inout STD_LOGIC;
    DDR_we_n : inout STD_LOGIC;
    DDR_ba : inout STD_LOGIC_VECTOR ( 2 downto 0 );
    DDR_addr : inout STD_LOGIC_VECTOR ( 14 downto 0 );
    DDR_dm : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_dq : inout STD_LOGIC_VECTOR ( 31 downto 0 );
    DDR_dqs_n : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_dqs_p : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    FIXED_IO_mio : inout STD_LOGIC_VECTOR ( 53 downto 0 );
    FIXED_IO_ddr_vrn : inout STD_LOGIC;
    FIXED_IO_ddr_vrp : inout STD_LOGIC;
    FIXED_IO_ps_srstb : inout STD_LOGIC;
    FIXED_IO_ps_clk : inout STD_LOGIC;
    FIXED_IO_ps_porb : inout STD_LOGIC
  );
  end component ATLASPix3BD;
begin
ATLASPix3BD_i: component ATLASPix3BD
     port map (
      CLK_EXT_N(0) => CLK_EXT_N(0),
      CLK_EXT_P(0) => CLK_EXT_P(0),
      CLK_REF_N(0) => CLK_REF_N(0),
      CLK_REF_P(0) => CLK_REF_P(0),
      CLK_SI_8_clk_n => CLK_SI_8_clk_n,
      CLK_SI_8_clk_p => CLK_SI_8_clk_p,
      DDR_addr(14 downto 0) => DDR_addr(14 downto 0),
      DDR_ba(2 downto 0) => DDR_ba(2 downto 0),
      DDR_cas_n => DDR_cas_n,
      DDR_ck_n => DDR_ck_n,
      DDR_ck_p => DDR_ck_p,
      DDR_cke => DDR_cke,
      DDR_cs_n => DDR_cs_n,
      DDR_dm(3 downto 0) => DDR_dm(3 downto 0),
      DDR_dq(31 downto 0) => DDR_dq(31 downto 0),
      DDR_dqs_n(3 downto 0) => DDR_dqs_n(3 downto 0),
      DDR_dqs_p(3 downto 0) => DDR_dqs_p(3 downto 0),
      DDR_odt => DDR_odt,
      DDR_ras_n => DDR_ras_n,
      DDR_reset_n => DDR_reset_n,
      DDR_we_n => DDR_we_n,
      FIXED_IO_ddr_vrn => FIXED_IO_ddr_vrn,
      FIXED_IO_ddr_vrp => FIXED_IO_ddr_vrp,
      FIXED_IO_mio(53 downto 0) => FIXED_IO_mio(53 downto 0),
      FIXED_IO_ps_clk => FIXED_IO_ps_clk,
      FIXED_IO_ps_porb => FIXED_IO_ps_porb,
      FIXED_IO_ps_srstb => FIXED_IO_ps_srstb,
      led_0 => led_0,
      miso_0 => miso_0,
      mosi_0 => mosi_0,
      reset => reset,
      spi_clk_0 => spi_clk_0,
      ss_0(0) => ss_0(0)
    );
end STRUCTURE;
