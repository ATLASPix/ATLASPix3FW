proc numberOfCPUs {} {
    # Windows puts it in an environment variable
    global tcl_platform env
    if {$tcl_platform(platform) eq "windows"} {
        return $env(NUMBER_OF_PROCESSORS)
    }

    # Check for sysctl (OSX, BSD)
    set sysctl [auto_execok "sysctl"]
    if {[llength $sysctl]} {
        if {![catch {exec {*}$sysctl -n "hw.ncpu"} cores]} {
            return $cores
        }
    }

    # Assume Linux, which has /proc/cpuinfo, but be careful
    if {![catch {open "/proc/cpuinfo"} f]} {
        set cores [regexp -all -line {^processor\s} [read $f]]
        close $f
        if {$cores > 0} {
            return $cores
        }
    }

    # No idea what the actual number of cores is; exhausted all our options
    # Fall back to returning 1; there must be at least that because we're running on it!
    return 1
}


set multiCore [numberOfCPUs]
set outputDir outputs
set cur_impl [format "impl_1"]

puts "----------------------Opening ATLASPix3FW project------------------------------------"
open_project ATLASPix3FW/ATLASPix3FW.xpr

# Run implementation
reset_run $cur_impl
launch_runs $cur_impl -jobs $multiCore
wait_on_run $cur_impl

#Generate JUnit result file
tclapp::install junit -quiet
package require ::tclapp::xilinx::junit
namespace import ::tclapp::xilinx::junit::*
set_report ./report_impl.xml
process_runs [ get_runs impl_1 ] 
write_results

set fo [open "metrics.txt" "w"] 
set WNS [get_property STATS.WNS [current_run]] 
set TNS [get_property STATS.TNS [current_run]] 
set WHS [get_property STATS.WHS [current_run]] 
set TPWS [get_property STATS.TPWS [current_run]] 

puts $fo [format "WNS  %f\n" $WNS]
puts $fo [format "TNS  %f\n" $TNS]
puts $fo [format "WHS  %f\n" $WHS]
puts $fo [format "TPWS  %f\n" $TPWS]

close $fo

puts "Implementation done !"

 

