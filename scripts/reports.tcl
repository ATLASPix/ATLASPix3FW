set outputDir outputs

puts "----------------------Opening ATLASPix3FW project------------------------------------"
open_project ATLASPix3FW/ATLASPix3FW.xpr

open_impl_design

# Reports
report_route_status -file $outputDir/post_route_status.rpt
report_timing_summary -file $outputDir/post_route_timing_summary.rpt
report_power -file $outputDir/post_route_power.rpt
report_drc -file $outputDir/post_imp_drc.rpt

close_design
