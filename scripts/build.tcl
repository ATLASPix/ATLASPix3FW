proc numberOfCPUs {} {
    # Windows puts it in an environment variable
    global tcl_platform env
    if {$tcl_platform(platform) eq "windows"} {
        return $env(NUMBER_OF_PROCESSORS)
    }

    # Check for sysctl (OSX, BSD)
    set sysctl [auto_execok "sysctl"]
    if {[llength $sysctl]} {
        if {![catch {exec {*}$sysctl -n "hw.ncpu"} cores]} {
            return $cores
        }
    }

    # Assume Linux, which has /proc/cpuinfo, but be careful
    if {![catch {open "/proc/cpuinfo"} f]} {
        set cores [regexp -all -line {^processor\s} [read $f]]
        close $f
        if {$cores > 0} {
            return $cores
        }
    }

    # No idea what the actual number of cores is; exhausted all our options
    # Fall back to returning 1; there must be at least that because we're running on it!
    return 1
}




set multiCore [numberOfCPUs]
set outputDir outputs


puts "----------------------Opening ATLASPix3FW project------------------------------------"
open_project ATLASPix3FW/ATLASPix3FW.xpr

reset_target all [get_files  "ATLASPix3FW/ATLASPix3FW.srcs/sources_1/bd/ATLASPix3BD/ATLASPix3BD.bd"]
generate_target all [get_files  "ATLASPix3FW/ATLASPix3FW.srcs/sources_1/bd/ATLASPix3BD/ATLASPix3BD.bd"]
upgrade_ip [get_ips]


# Run synthesis
reset_run synth_1
launch_runs synth_1 -jobs $multiCore
wait_on_run synth_1
puts "Synthesis done !"

# Run implementation
reset_run impl_1
launch_runs impl_1 -jobs $multiCore
wait_on_run impl_1
puts "Implementation done !"

open_impl_design

# Reports
report_route_status -file $outputDir/post_route_status.rpt
report_timing_summary -file $outputDir/post_route_timing_summary.rpt
report_power -file $outputDir/post_route_power.rpt
report_drc -file $outputDir/post_imp_drc.rpt

# Write bitstream
#set_property BITSTREAM.CONFIG.USR_ACCESS TIMESTAMP [current_design]
write_bitstream -force $outputDir/ATLASPix3FW_latest.bit
write_debug_probes -force $outputDir/debug_probes.ltx
puts "Bitfile generated !"


close_design

# Write hadware definition
write_hwdef -force  -file $outputDir/ATLASPix3FW_top.hdf
puts "Hardware definition created !"

