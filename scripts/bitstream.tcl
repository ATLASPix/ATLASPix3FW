set outputDir outputs

puts "----------------------Opening ATLASPix3FW project------------------------------------"
open_project ATLASPix3FW/ATLASPix3FW.xpr

open_impl_design

# Write bitstream
#set_property BITSTREAM.CONFIG.USR_ACCESS TIMESTAMP [current_design]
write_bitstream -force $outputDir/ATLASPix3FW_latest.bit
write_debug_probes -force $outputDir/debug_probes.ltx
puts "Bitfile generated !"


close_design
